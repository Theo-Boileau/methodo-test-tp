package com.ynov.commerce.business.interf;

import com.ynov.commerce.dto.SessionDto;
import com.ynov.commerce.dto.UserDto;
import com.ynov.commerce.dto.UserDtoLight;
import com.ynov.commerce.entities.Session;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
public interface UserServiceInterf {

    List<UserDto> getUsers();

    UUID createUser(UserDtoLight userDtoLight) throws Exception;

    SessionDto getSession(UUID sessionID) throws Exception;

    Set<SessionDto> getSessions(UUID userID) throws Exception;

    UUID startSession(UUID userId) throws Exception;

}
