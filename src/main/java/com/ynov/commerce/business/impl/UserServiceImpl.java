package com.ynov.commerce.business.impl;

import com.ynov.commerce.business.interf.UserServiceInterf;
import com.ynov.commerce.dto.SessionDto;
import com.ynov.commerce.dto.UserDto;
import com.ynov.commerce.dto.UserDtoLight;
import com.ynov.commerce.entities.Session;
import com.ynov.commerce.entities.User;
import com.ynov.commerce.mappers.SessionMapper;
import com.ynov.commerce.mappers.UserMapper;
import com.ynov.commerce.repositories.SessionRepository;
import com.ynov.commerce.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserServiceInterf {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SessionRepository sessionRepository;
    private final UserMapper userMapper = UserMapper.instance;

    private final SessionMapper sessionMapper = SessionMapper.instance;

    @Override
    public List<UserDto> getUsers() {
        return userMapper.toUserDtoList(userRepository.findAll());
    }

    @Override
    public UUID createUser(UserDtoLight userDtoLight){
        User user = new User();
        user.setNom(userDtoLight.getNom());
        User createdUser = userRepository.save(user);
        return createdUser.getId();
    }

    @Override
    public SessionDto getSession(UUID sessionID) throws Exception{
        if(!sessionRepository.existsById(sessionID)){
           throw new Exception("No session found for this ID");
        }
        return sessionRepository.findById(sessionID)
                .map(sessionMapper::toSessionDto)
                .orElse(null);
    }

    @Override
    public Set<SessionDto> getSessions(UUID userID)throws Exception{
        if(!userRepository.existsById(userID)){
            throw new Exception("User doesn't exist");
        }

        if(sessionMapper.toSessionDtoList(sessionRepository.findByUser_Id(userID)).isEmpty()){
            throw new Exception("No session found for the current user");
        }
        return sessionMapper.toSessionDtoList(sessionRepository.findByUser_Id(userID));
    }

    @Override
    public UUID startSession(UUID userId) throws Exception {
        if(!userRepository.existsById(userId)){
            throw new Exception("User doesn't exist");
        }

        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formattedDateTime = now.format(formatter);
        LocalDateTime formattedLocalDateTime = LocalDateTime.parse(formattedDateTime, formatter);

        Session session = new Session();
        session.setStart(formattedLocalDateTime);

        Session createdSession = sessionRepository.save(session);
        return createdSession.getId();
    }

}
