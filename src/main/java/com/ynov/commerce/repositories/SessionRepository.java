package com.ynov.commerce.repositories;

import com.ynov.commerce.entities.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;
import java.util.UUID;

@Repository
public interface SessionRepository extends JpaRepository<Session, UUID> {
    Set<Session> findByUser_Id(UUID id);
    @Override
    boolean existsById(UUID uuid);
}
