package com.ynov.commerce.mappers;

import com.ynov.commerce.dto.SessionDto;
import com.ynov.commerce.entities.Session;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.Set;

@Mapper
public interface SessionMapper {

    SessionMapper instance = Mappers.getMapper(SessionMapper.class);

    SessionDto toSessionDto(Session session);

    @Mapping(target = "id", ignore = true)
    Session toSession(SessionDto sessionDto);

    Set<SessionDto> toSessionDtoList(Set<Session> sessions);

}
