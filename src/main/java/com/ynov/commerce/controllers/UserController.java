package com.ynov.commerce.controllers;


import com.ynov.commerce.business.impl.UserServiceImpl;
import com.ynov.commerce.dto.UserDto;
import com.ynov.commerce.dto.UserDtoLight;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@Slf4j
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    @GetMapping
    public ResponseEntity<?> getUsers(){

        List<UserDto> result = userService.getUsers();

        if(result.isEmpty()) return ResponseEntity.noContent().build();

        return ResponseEntity.ok(result);
    }

    @PostMapping
    public ResponseEntity<?> createUser(@RequestBody UserDtoLight userDtoLight){
        userService.createUser(userDtoLight);
        return ResponseEntity.ok("L'utilisateur " + userDtoLight.getNom() + " à été créé");
    }
}
