package com.ynov.commerce.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.annotation.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
public class SessionDto {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("start")
    private LocalDateTime start;

    @JsonProperty("end")
    @Nullable
    private LocalDateTime end;

    @JsonProperty("pause")
    private LocalDateTime pause;
}
