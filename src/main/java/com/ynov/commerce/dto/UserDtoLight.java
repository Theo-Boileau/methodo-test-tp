package com.ynov.commerce.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserDtoLight {

    @JsonProperty("nom")
    private String nom;

}
