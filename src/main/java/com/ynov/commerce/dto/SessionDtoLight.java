package com.ynov.commerce.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.annotation.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class SessionDtoLight {

    @JsonProperty("start")
    private LocalDateTime start;

    @JsonProperty("end")
    @Nullable
    private LocalDateTime end;
}
