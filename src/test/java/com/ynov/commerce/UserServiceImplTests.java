package com.ynov.commerce;


import com.ynov.commerce.business.impl.UserServiceImpl;
import com.ynov.commerce.dto.SessionDto;
import com.ynov.commerce.entities.Session;
import com.ynov.commerce.entities.User;
import com.ynov.commerce.repositories.SessionRepository;
import com.ynov.commerce.repositories.UserRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class UserServiceImplTests {

    @Mock
    private UserRepository userRepository;

    @Mock
    private SessionRepository sessionRepository;

    @InjectMocks
    private UserServiceImpl userService;

    private Set<Session> sessionList = new HashSet<>();
    private User user = new User(UUID.randomUUID(), "BOBRIS", sessionList);

    private LocalDateTime start = LocalDateTime.of(2024, 11, 13, 12, 30);
    private LocalDateTime end = LocalDateTime.of(2024, 11, 13, 17, 30);

    private Session session1 = new Session(UUID.randomUUID(), start, end, user);
    private Session session2 = new Session(UUID.randomUUID(), start, end, user);


    @Test
    @DisplayName("getSession by Id of User correct")
    void getSessionCorrect() throws Exception {
        //given
        sessionList.add(session1);
        //When
        Mockito.when(sessionRepository.findById(session1.getId()))
                .thenReturn(Optional.of(session1));
        SessionDto  result = userService.getSession(session1.getId());
        //Then
        //Make sure to import assertThat From  package
        assertNotNull(result);
        assertEquals(result.getId(),session1.getId());
    }

    @Test
    @DisplayName("GetSession by id exception session not found")
    void getSessionUncorrect() throws Exception {
        //given
        sessionList.add(session1);
        //When
        Mockito.when(sessionRepository.findById(UUID.fromString("0")))
                .thenThrow(new Exception("No session found for this ID"));
        SessionDto  result = userService.getSession(UUID.fromString("0"));
        //Then
        //Make sure to import assertThat From  package

    }

    @Test
    @DisplayName("GetSessionsList by id exception session not found")
    void getSessionsCorrect() throws Exception {
        //given
        sessionList.add(session1);
        sessionList.add(session2);
        //When
        Mockito.when(sessionRepository.findAll())
                .thenReturn((List<Session>) sessionList);


        Set<SessionDto>  resultList = userService.getSessions(user.getId());
        //Then
        //Make sure to import assertThat From  package
        assertNotNull(resultList);
        assertEquals(resultList, sessionList);
    }

    @Test
    @DisplayName("Test if startSession is ok")
    void startSessionTestIfOk() throws Exception {

        //Given
        UUID userId = user.getId();
        UUID sessionId = session1.getId();

        //When
        Mockito.when(userRepository.existsById(any(UUID.class)))
                        .thenReturn(true);

        Mockito.when(sessionRepository.save(any(Session.class)))
                .thenReturn(session1);

        //Then
        UUID createdSessionId = userService.startSession(userId);

        verify(userRepository).existsById(userId);

        verify(sessionRepository).save(argThat(session -> session.getStart() != null));

        assertEquals(sessionId, createdSessionId);
    }

    @Test
    @DisplayName("Test if startSession is not ok because of user not existing")
    void startSessionTestIfUserNotExist() throws Exception {

        //Given
        UUID userId = user.getId();
        UUID sessionId = session1.getId();

        //When
        Mockito.when(userRepository.existsById(any(UUID.class)))
                .thenReturn(false);

        //Then
        Mockito.doThrow(new Exception("User doesn't exist")).when(userService)
                .startSession(any(UUID.class));

        assertThrows(Exception.class, () -> userService.startSession(userId));
    }

    @Test
    @DisplayName("Test if pauseSession is ok")
    void pauseSession() throws Exception {

        //Given
        UUID userId = user.getId();
        UUID sessionId = session1.getId();

        //When
        Mockito.when(userRepository.existsById(any(UUID.class)))
                .thenReturn(true);

        Mockito.when(sessionRepository.save(any(Session.class)))
                .thenReturn(session1);

        //Then
        UUID createdSessionId = userService.startSession(userId);

        verify(userRepository).existsById(userId);

        verify(sessionRepository).save(argThat(session -> session.getStart() != null));

        assertEquals(sessionId, createdSessionId);
    }

}
